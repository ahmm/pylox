from typing import Any

from pylox.exceptions import LoxException


class Return(LoxException):
    def __init__(self, value: Any):
        self.value = value
