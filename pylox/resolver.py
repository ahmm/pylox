from collections import deque
from enum import Enum, auto

from pylox.exceptions import ResolveError
from pylox.expr import (
    AssignmentExpr,
    BinaryExpr,
    CallExpr,
    Expr,
    GroupingExpr,
    LiteralExpr,
    LogicalExpr,
    UnaryExpr,
    VariableExpr,
)
from pylox.stmt import (
    BlockStmt,
    ExpressionStmt,
    FunctionStmt,
    IfStmt,
    PrintStmt,
    ReturnStmt,
    Stmt,
    VarStmt,
    WhileStmt,
)
from pylox.token_type import Token


class FunctionType(Enum):
    NONE = auto()
    FUNCTION = auto()


class Resolver:
    def __init__(self):
        self.scopes: deque[dict] = deque()
        self.current_function: FunctionType = FunctionType.NONE

        self.distances: dict[Expr, int] = {}

    def resolve(self, statements: list[Stmt]) -> dict[Expr, int]:
        self._resolve_stmt_list(statements)
        return self.distances

    def _resolve_stmt_list(self, statements: list[Stmt]) -> None:
        for stmt in statements:
            self._resolve_stmt(stmt)

    def _resolve_stmt(self, stmt: Stmt) -> None:
        match stmt:
            case BlockStmt():
                self._resolve_block_stmt(stmt)
            case VarStmt():
                self._resolve_var_stmt(stmt)
            case FunctionStmt():
                self._resolve_function_stmt(stmt)
            case ExpressionStmt():
                self._resolve_expression_stmt(stmt)
            case IfStmt():
                self._resolve_if_stmt(stmt)
            case PrintStmt():
                self._resolve_print_stmt(stmt)
            case ReturnStmt():
                self._resolve_return_stmt(stmt)
            case WhileStmt():
                self._resolve_while_stmt(stmt)

    def _resolve_block_stmt(self, stmt: BlockStmt) -> None:
        self._begin_scope()
        self._resolve_stmt_list(stmt.statements)
        self._end_scope()

    def _resolve_var_stmt(self, stmt: VarStmt) -> None:
        self._declare(stmt.name)
        if stmt.initializer:
            self._resolve_expr(stmt.initializer)
        self._define(stmt.name)

    def _resolve_function_stmt(self, stmt: FunctionStmt) -> None:
        # Resolve the function name itself:
        self._declare(stmt.name)
        self._define(stmt.name)

        enclosing_function = self.current_function
        self.current_function = FunctionType.FUNCTION

        # Resolve the function body:
        self._begin_scope()
        for parameter in stmt.parameters:
            self._declare(parameter)
            self._define(parameter)
        self._resolve_stmt_list(stmt.body)
        self._end_scope()

        self.current_function = enclosing_function

    def _resolve_expression_stmt(self, stmt: ExpressionStmt) -> None:
        self._resolve_expr(stmt.expression)

    def _resolve_if_stmt(self, stmt: IfStmt) -> None:
        self._resolve_expr(stmt.condition)
        self._resolve_stmt(stmt.then_branch)
        if stmt.else_branch:
            self._resolve_stmt(stmt.else_branch)

    def _resolve_print_stmt(self, stmt: PrintStmt) -> None:
        self._resolve_expr(stmt.expression)

    def _resolve_return_stmt(self, stmt: ReturnStmt) -> None:
        if self.current_function == FunctionType.NONE:
            raise ResolveError(stmt.keyword, "Can't return from top-level code.")

        if stmt.value:
            self._resolve_expr(stmt.value)

    def _resolve_while_stmt(self, stmt: WhileStmt) -> None:
        self._resolve_expr(stmt.condition)
        self._resolve_stmt(stmt.body)

    def _resolve_expr(self, expr: Expr) -> None:
        match expr:
            case VariableExpr():
                self._resolve_variable_expr(expr)
            case AssignmentExpr():
                self._resolve_assignment_expr(expr)
            case BinaryExpr():
                self._resolve_binary_expr(expr)
            case CallExpr():
                self._resolve_call_expr(expr)
            case GroupingExpr():
                self._resolve_grouping_expr(expr)
            case LogicalExpr():
                self._resolve_logical_expr(expr)
            case UnaryExpr():
                self._resolve_unary_expr(expr)
            case LiteralExpr():
                pass  # no need to do anything for literals

    def _resolve_variable_expr(self, expr: VariableExpr) -> None:
        if len(self.scopes) and self.scopes[-1].get(expr.name.lexeme) is False:
            raise ResolveError(expr.name, "Can't read local variable in its own initializer")

        self._resolve_local(expr, expr.name)

    def _resolve_assignment_expr(self, expr: AssignmentExpr) -> None:
        self._resolve_expr(expr.value)
        self._resolve_local(expr, expr.name)

    def _resolve_binary_expr(self, expr: BinaryExpr) -> None:
        self._resolve_expr(expr.left)
        self._resolve_expr(expr.right)

    def _resolve_call_expr(self, expr: CallExpr) -> None:
        self._resolve_expr(expr.callee)
        for argument in expr.arguments:
            self._resolve_expr(argument)

    def _resolve_grouping_expr(self, expr: GroupingExpr) -> None:
        self._resolve_expr(expr.expression)

    def _resolve_logical_expr(self, expr: LogicalExpr) -> None:
        self._resolve_expr(expr.left)
        self._resolve_expr(expr.right)

    def _resolve_unary_expr(self, expr: UnaryExpr) -> None:
        self._resolve_expr(expr.right)

    def _resolve_local(self, expr: Expr, name: Token) -> None:
        """
        Save the "distance" of the variable from the current scope, e.g. if the variable was found in the
        current scope = 0, if it's in the immediately enclosing scope - 1 etc.
        """
        for distance, scope in enumerate(reversed(self.scopes)):
            if name.lexeme in scope:
                self.distances[expr] = distance
                return

    # helper methods
    def _begin_scope(self) -> None:
        self.scopes.append({})

    def _end_scope(self) -> None:
        self.scopes.pop()

    def _declare(self, name: Token) -> None:
        if not len(self.scopes):
            return

        if name.lexeme in self.scopes[-1]:
            raise ResolveError(name, "Already a variable with this name in this scope.")

        self.scopes[-1][name.lexeme] = False

    def _define(self, name: Token) -> None:
        if len(self.scopes):
            self.scopes[-1][name.lexeme] = True
