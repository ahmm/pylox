from abc import ABC
from dataclasses import dataclass
from typing import Any

from pylox.token_type import Token


class Expr(ABC):
    pass


@dataclass(frozen=True)
class BinaryExpr(Expr):
    left: Expr
    operator: Token
    right: Expr


@dataclass(frozen=True)
class LogicalExpr(Expr):
    """Logical expressions get their own dataclass because they short-circuit."""

    left: Expr
    operator: Token
    right: Expr


@dataclass(frozen=True)
class LiteralExpr(Expr):
    value: Any


@dataclass(frozen=True)
class GroupingExpr(Expr):
    expression: Expr


@dataclass(frozen=True)
class UnaryExpr(Expr):
    operator: Token
    right: Expr


@dataclass(frozen=True)
class VariableExpr(Expr):
    name: Token


@dataclass(frozen=True)
class AssignmentExpr(Expr):
    name: Token
    value: Expr


@dataclass(frozen=True)
class CallExpr(Expr):
    callee: Expr
    closing_parenthesis: Token
    arguments: list[Expr]
