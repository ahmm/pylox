class Singleton(type):
    _instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


# We don't want the definition of what an identifier, or a number is to depend on what
# Python considers them to be. So we provide our own implementation of these basic functions.


def is_digit(char: str) -> bool:
    return "0" <= char <= "9"


def is_alpha(char: str) -> bool:
    return ("a" <= char <= "z") or ("A" <= char <= "Z") or char == "_"


def is_alpha_numeric(char: str) -> bool:
    return is_digit(char) or is_alpha(char)
