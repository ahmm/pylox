import argparse
import pathlib

from pylox.lox import Lox


def main():
    parser = argparse.ArgumentParser(description="Lox interpreter")
    parser.add_argument("script", type=pathlib.Path, nargs="?")

    args = parser.parse_args()

    lox = Lox()

    if args.script:
        lox.run_file(args.script)
    else:
        lox.run_prompt()


if __name__ == "__main__":
    main()
