from abc import ABC
from dataclasses import dataclass
from typing import Optional

from pylox.expr import Expr
from pylox.token_type import Token


class Stmt(ABC):
    pass


@dataclass(frozen=True)
class PrintStmt(Stmt):
    expression: Expr


@dataclass(frozen=True)
class ExpressionStmt(Stmt):
    expression: Expr


@dataclass(frozen=True)
class VarStmt(Stmt):
    name: Token
    initializer: Optional[Expr]


@dataclass(frozen=True)
class BlockStmt(Stmt):
    statements: list[Stmt]


@dataclass(frozen=True)
class IfStmt(Stmt):
    condition: Expr
    then_branch: Stmt
    else_branch: Optional[Stmt]


@dataclass(frozen=True)
class WhileStmt(Stmt):
    condition: Expr
    body: Stmt


@dataclass(frozen=True)
class FunctionStmt(Stmt):
    name: Token
    parameters: list[Token]
    body: list[Stmt]


@dataclass(frozen=True)
class ReturnStmt(Stmt):
    keyword: Token
    value: Expr | None
