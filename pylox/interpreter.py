from typing import Any, Final

from pylox import builtin_functions
from pylox.environment import Environment
from pylox.exceptions import DivisionByZeroError, RuntimeError
from pylox.expr import (
    AssignmentExpr,
    BinaryExpr,
    CallExpr,
    Expr,
    GroupingExpr,
    LiteralExpr,
    LogicalExpr,
    UnaryExpr,
    VariableExpr,
)
from pylox.function import Callable, Function
from pylox.return_exception import Return
from pylox.stmt import (
    BlockStmt,
    ExpressionStmt,
    FunctionStmt,
    IfStmt,
    PrintStmt,
    ReturnStmt,
    Stmt,
    VarStmt,
    WhileStmt,
)
from pylox.token_type import Token, TokenType


def is_truthy(value: Any) -> bool:
    if value is None:
        return False
    if isinstance(value, bool):
        return value

    return True


def is_equal(a: Any, b: Any) -> bool:
    if a is None and b is None:
        return True
    if a is None or b is None:
        return False
    return type(a) == type(b) and a == b


def validate_numeric_operands(operator: Token, left: Any, right: Any) -> None:
    if isinstance(left, float) and isinstance(right, float):
        return
    raise RuntimeError(operator, "Operands must be numbers.")


def stringify(value: Any) -> str:
    match value:
        case None:
            return "nil"
        case True:
            return "true"
        case False:
            return "false"
        case float() if int(value) == value:
            return str(int(value))
        case anything_else:
            return str(anything_else)


class Interpreter:
    def __init__(self):
        self.global_environment: Final[Environment] = Environment()
        self.environment = self.global_environment
        self.locals: dict[Expr, int] = {}

        self._define_builtins()

    def interpret(self, statements: list[Stmt]) -> None:
        for statement in statements:
            self.execute(statement)

    def _define_builtins(self) -> None:
        self.global_environment.define("clock", builtin_functions.Clock())
        self.global_environment.define("print", builtin_functions.Print())

    def execute(self, stmt: Stmt) -> Any:
        match stmt:
            case IfStmt():
                return self.execute_if(stmt)
            case WhileStmt():
                return self.execute_while(stmt)
            case FunctionStmt():
                return self.execute_function(stmt)
            case PrintStmt():
                return self.execute_print(stmt)
            case ReturnStmt():
                return self.execute_return(stmt)
            case VarStmt():
                return self.execute_var(stmt)
            case ExpressionStmt(expression):
                return self.evaluate(expression)
            case BlockStmt(statements):
                self.execute_block(statements, Environment(self.environment))
            case _:
                raise Exception("Unsupported Stmt type")

    def evaluate(self, expr: Expr) -> Any:
        match expr:
            case LiteralExpr(value):
                return value
            case UnaryExpr():
                return self.evaluate_unary(expr)
            case CallExpr():
                return self.evaluate_call_expr(expr)
            case VariableExpr():
                return self.evaluate_variable_expr(expr)
            case AssignmentExpr():
                return self.evaluate_assignment(expr)
            case BinaryExpr():
                return self.evaluate_binary(expr)
            case LogicalExpr():
                return self.evaluate_logical(expr)
            case GroupingExpr(expression):
                return self.evaluate(expression)
            case _:
                raise Exception("Unsupported Expr type")

    # Methods for handling individual statement types
    def execute_if(self, stmt: IfStmt) -> None:
        if is_truthy(self.evaluate(stmt.condition)):
            self.execute(stmt.then_branch)
        elif stmt.else_branch is not None:
            self.execute(stmt.else_branch)

    def execute_while(self, stmt: WhileStmt) -> None:
        while is_truthy(self.evaluate(stmt.condition)):
            self.execute(stmt.body)

    def execute_function(self, stmt: FunctionStmt) -> None:
        function = Function(stmt)
        self.environment.define(stmt.name.lexeme, function)

    def execute_print(self, stmt: PrintStmt) -> None:
        value = self.evaluate(stmt.expression)
        print(stringify(value))

    def execute_return(self, stmt: ReturnStmt) -> None:
        value = None
        if stmt.value is not None:
            value = self.evaluate(stmt.value)

        raise Return(value)

    def execute_var(self, stmt: VarStmt) -> None:
        value = self.evaluate(stmt.initializer) if stmt.initializer is not None else None
        self.environment.define(stmt.name.lexeme, value)

    def execute_block(self, statements: list[Stmt], environment: Environment) -> None:
        previous_environment = self.environment

        try:
            self.environment = environment
            for stmt in statements:
                self.execute(stmt)
        finally:
            self.environment = previous_environment

    # Methods for handling individual expression types
    def evaluate_unary(self, expr: UnaryExpr) -> Any:
        right = self.evaluate(expr.right)

        match expr.operator.token_type:
            case TokenType.MINUS:
                if not isinstance(right, float):
                    raise RuntimeError(expr.operator, "Operand must be a number.")
                return -right
            case TokenType.BANG:
                return not is_truthy(right)

    def evaluate_assignment(self, expr: AssignmentExpr) -> Expr:
        value = self.evaluate(expr.value)

        distance = self.locals.get(expr)
        if distance is not None:
            self.environment.set_at(distance, expr.name, value)
        else:
            self.global_environment[expr.name] = value

        return value

    def evaluate_call_expr(self, expr: CallExpr) -> Any:
        callable: Callable = self.evaluate(expr.callee)

        arguments = []
        for argument in expr.arguments:
            arguments.append(self.evaluate(argument))

        if not issubclass(type(callable), Callable):
            raise RuntimeError(expr.closing_parenthesis, "Can only call functions and classes.")
        if len(arguments) != callable.arity:
            raise RuntimeError(
                expr.closing_parenthesis,
                f"Expected {callable.arity} arguments but got {len(arguments)}.",
            )

        return callable.call(self, arguments)

    def evaluate_variable_expr(self, expr: VariableExpr) -> Any:
        return self._lookup_variable(expr.name, expr)

    def evaluate_binary(self, expr: BinaryExpr) -> Any:
        left = self.evaluate(expr.left)
        right = self.evaluate(expr.right)

        match expr.operator.token_type:
            case TokenType.MINUS:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) - float(right)
            case TokenType.SLASH:
                validate_numeric_operands(expr.operator, left, right)
                if right == 0:
                    raise DivisionByZeroError(expr.operator, "Division by zero.")
                return float(left) / float(right)
            case TokenType.STAR:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) * float(right)
            case TokenType.PLUS:
                if isinstance(left, float) and isinstance(right, float):
                    return float(left) + float(right)
                if isinstance(left, str) and isinstance(right, str):
                    return str(left) + str(right)
                raise RuntimeError(expr.operator, "Operands must be two numbers or two strings.")
            case TokenType.GREATER:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) > float(right)
            case TokenType.GREATER_EQUAL:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) >= float(right)
            case TokenType.LESS:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) < float(right)
            case TokenType.LESS_EQUAL:
                validate_numeric_operands(expr.operator, left, right)
                return float(left) <= float(right)
            case TokenType.BANG_EQUAL:
                return not is_equal(left, right)
            case TokenType.EQUAL_EQUAL:
                return is_equal(left, right)

    def evaluate_logical(self, expr: LogicalExpr) -> Any:
        left = self.evaluate(expr.left)

        if expr.operator.token_type == TokenType.OR and is_truthy(left):
            return left
        elif expr.operator.token_type == TokenType.AND and not is_truthy(left):
            return left

        return self.evaluate(expr.right)

    def _lookup_variable(self, name: Token, expr: Expr) -> Any:
        distance = self.locals.get(expr)
        if distance is not None:
            return self.environment.get_at(distance, name)
        return self.global_environment[name]
