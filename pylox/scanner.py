from typing import Any, Final, Optional

from pylox.token_type import Token, TokenType
from pylox.utils import is_alpha, is_alpha_numeric, is_digit

RESERVED_KEYWORDS: Final[dict] = {
    "and": TokenType.AND,
    "class": TokenType.CLASS,
    "else": TokenType.ELSE,
    "false": TokenType.FALSE,
    "for": TokenType.FOR,
    "fun": TokenType.FUN,
    "if": TokenType.IF,
    "nil": TokenType.NIL,
    "or": TokenType.OR,
    "print": TokenType.PRINT,
    "return": TokenType.RETURN,
    "super": TokenType.SUPER,
    "this": TokenType.THIS,
    "true": TokenType.TRUE,
    "var": TokenType.VAR,
    "while": TokenType.WHILE,
}


class Scanner:
    def __init__(self, source: str):
        self.source = source

        self.start = 0  # set in scan tokens where we're advancing
        self.current = 0  # diverges from 'start' in lookaheads and so on
        self.line = 1

        self.tokens: list[Token] = []
        self.errors: list[str] = []

    def scan_tokens(self) -> tuple[list[Token], list[str]]:
        """Perform lexical analysis on the source code, returning a list of Tokens and errors"""
        while not self.is_at_end():
            self.start = self.current
            self.scan_token()
        self.tokens.append(Token(TokenType.EOF, "", self.line))
        return self.tokens, self.errors

    def scan_token(self) -> None:
        c = self.advance()

        match c:
            # Match simple single-character lexemes
            case "(":
                self.add_token(TokenType.LEFT_PAREN)
            case ")":
                self.add_token(TokenType.RIGHT_PAREN)
            case "{":
                self.add_token(TokenType.LEFT_BRACE)
            case "}":
                self.add_token(TokenType.RIGHT_BRACE)
            case ",":
                self.add_token(TokenType.COMMA)
            case ".":
                self.add_token(TokenType.DOT)
            case "-":
                self.add_token(TokenType.MINUS)
            case "+":
                self.add_token(TokenType.PLUS)
            case ";":
                self.add_token(TokenType.SEMICOLON)
            case "*":
                self.add_token(TokenType.STAR)

            # Match !, =, >, < or their equivalents !=, ==, >=, <=
            case "!":
                self.add_token(TokenType.BANG_EQUAL if self.match("=") else TokenType.BANG)
            case "=":
                self.add_token(TokenType.EQUAL_EQUAL if self.match("=") else TokenType.EQUAL)
            case ">":
                self.add_token(TokenType.GREATER_EQUAL if self.match("=") else TokenType.GREATER)
            case "<":
                self.add_token(TokenType.LESS_EQUAL if self.match("=") else TokenType.LESS)

            # Match division / and comments //
            case "/":
                # Discard comment until the end of line
                if self.match("/"):
                    while not self.is_at_end() and self.peek() != "\n":
                        self.advance()
                elif self.match("*"):
                    self.handle_multiline_comment()

                else:
                    self.add_token(TokenType.SLASH)

            # Skip whitespace
            case " " | "\r" | "\t":
                pass

            case "\n":
                self.line += 1

            # Literals
            case '"':
                self.handle_string()

            case _:
                if is_digit(c):
                    self.handle_number()
                elif is_alpha(c):
                    self.handle_identifier()
                else:
                    self.errors.append(f"[Line {self.line}] Error: Unexpected character '{c}'")

    # Handling specific lexemes
    def handle_multiline_comment(self) -> None:
        # consume until we get */
        while self.current + 1 < len(self.source) and not (
            self.peek() == "*" and self.peek_next() == "/"
        ):
            if self.peek() == "\n":
                self.line += 1
            self.advance()

        if self.current + 1 >= len(self.source):
            self.errors.append(f"[Line {self.line}] Error: Unterminated multiline comment")
            self.advance()  # consume the last character so that we don't scan it
            return

        self.advance()  # consume *
        self.advance()  # consume /

    def handle_string(self) -> None:
        # Consume characters until we hit "
        while not self.is_at_end() and self.peek() != '"':
            if self.peek() == "\n":
                self.line += 1
            self.advance()

        if self.is_at_end():
            self.errors.append(f"[Line {self.line}] Error: Unterminated string")
            return

        # Consume the closing "
        self.advance()

        string_without_quotes = self.source[self.start + 1 : self.current - 1]
        self.add_token(TokenType.STRING, string_without_quotes)

    def handle_number(self) -> None:
        while is_digit(self.peek()):
            self.advance()

        # Look for a fractional part
        if self.peek() == "." and is_digit(self.peek_next()):
            # Consume the .
            self.advance()

            while is_digit(self.peek()):
                self.advance()

        self.add_token(TokenType.NUMBER, float(self.source[self.start : self.current]))

    def handle_identifier(self) -> None:
        while is_alpha_numeric(self.peek()):
            self.advance()

        text = self.source[self.start : self.current]
        token_type = RESERVED_KEYWORDS.get(text, TokenType.IDENTIFIER)
        self.add_token(token_type)

    # Utility functions
    def is_at_end(self) -> bool:
        return self.current >= len(self.source)

    def advance(self) -> str:
        """
        Returns the current char and moves forward in the source file,
        i.e. 'consumes the current character'.
        """
        c = self.source[self.current]
        self.current += 1
        return c

    def match(self, expected: str) -> bool:
        """Checks if the current char matches the expected one. If it does move forward."""
        if self.is_at_end():
            return False
        if self.source[self.current] != expected:
            return False

        self.current += 1
        return True

    def peek(self) -> str:
        """Lookahead, checks the current character without consuming it"""
        if self.is_at_end():
            return "\0"
        return self.source[self.current]

    def peek_next(self) -> str:
        """Lookahead, checks the *next* character without consuming any"""
        if self.current + 1 >= len(self.source):
            return "\0"
        return self.source[self.current + 1]

    def add_token(self, token_type: TokenType, literal: Optional[Any] = None) -> None:
        text = self.source[self.start : self.current]
        self.tokens.append(Token(token_type, text, self.line, self.current, literal))
