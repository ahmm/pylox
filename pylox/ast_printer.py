from pylox.expr import BinaryExpr, Expr, GroupingExpr, LiteralExpr, UnaryExpr
from pylox.token_type import Token, TokenType


class AstPrinter:
    def print(self, expr: Expr) -> str:
        return self._print_expr(expr)

    def _print_expr(self, expr: Expr):
        match expr:
            case LiteralExpr(value):
                return str(value) if value is not None else "nil"
            case UnaryExpr(operator, right):
                return self._parenthesize(operator.lexeme, right)
            case BinaryExpr(left, operator, right):
                return self._parenthesize(operator.lexeme, left, right)
            case GroupingExpr(expression):
                return self._parenthesize("group", expression)
            case _:
                raise Exception("Unsupported Expr type")

    def _parenthesize(self, name: str, *exprs: Expr) -> str:
        s = f"({name} "
        for expr in exprs:
            s += self._print_expr(expr) + " "
        s += ")"
        return s


if __name__ == "__main__":
    expr = BinaryExpr(
        UnaryExpr(Token(TokenType.MINUS, "-", 1), LiteralExpr(123)),
        Token(TokenType.STAR, "*", 1),
        GroupingExpr(LiteralExpr(45.67)),
    )

    print(AstPrinter().print(expr))
