from abc import ABC
from typing import Any

from pylox.environment import Environment
from pylox.return_exception import Return
from pylox.stmt import FunctionStmt


class Callable(ABC):
    @property
    def arity(self) -> int:
        raise NotImplementedError

    def call(self, *arguments: Any) -> Any:
        raise NotImplementedError


class Function(Callable):
    """Represents a user defined function that we can call."""

    def __init__(self, declaration: FunctionStmt):
        self.declaration = declaration

    @property
    def arity(self) -> int:
        return len(self.declaration.parameters)

    def call(self, interpreter, arguments: list[Any]) -> None:
        environment = Environment(interpreter.global_environment)

        for parameter, argument in zip(self.declaration.parameters, arguments):
            environment.define(parameter.lexeme, argument)

        try:
            interpreter.execute_block(self.declaration.body, environment)
        except Return as r:
            return r.value

    def __str__(self):
        return f"<fn {self.declaration.name.lexeme}>"
