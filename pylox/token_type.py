from dataclasses import dataclass
from enum import Enum
from typing import Any, Optional


class TokenType(Enum):
    # Single-character tokens.
    LEFT_PAREN = "LEFT_PAREN"
    RIGHT_PAREN = "RIGHT_PAREN"
    LEFT_BRACE = "LEFT_BRACE"
    RIGHT_BRACE = "RIGHT_BRACE"

    COMMA = "COMMA"
    DOT = "DOT"
    MINUS = "MINUS"
    PLUS = "PLUS"
    SEMICOLON = "SEMICOLON"
    SLASH = "SLASH"
    STAR = "STAR"

    # One or two character tokens.
    BANG = "BANG"
    BANG_EQUAL = "BANG_EQUAL"
    EQUAL = "EQUAL"
    EQUAL_EQUAL = "EQUAL_EQUAL"
    GREATER = "GREATER"
    GREATER_EQUAL = "GREATER_EQUAL"
    LESS = "LESS"
    LESS_EQUAL = "LESS_EQUAL"

    # Literals.
    IDENTIFIER = "IDENTIFIER"
    STRING = "STRING"
    NUMBER = "NUMBER"

    # Keywords.
    AND = "AND"
    CLASS = "CLASS"
    ELSE = "ELSE"
    FALSE = "FALSE"
    FUN = "FUN"
    FOR = "FOR"
    IF = "IF"
    NIL = "NIL"
    OR = "OR"

    PRINT = "PRINT"
    RETURN = "RETURN"
    SUPER = "SUPER"
    THIS = "THIS"
    TRUE = "TRUE"
    VAR = "VAR"
    WHILE = "WHILE"

    EOF = "EOF"


@dataclass(frozen=True)
class Token:
    token_type: TokenType
    lexeme: str
    line: int
    current: Optional[int] = None
    literal: Optional[Any] = None

    def __str__(self):
        if self.literal is not None:
            return f"{self.token_type.name} {self.lexeme} ({self.literal})"
        return f"{self.token_type.name} {self.lexeme}"

    def __repr__(self):
        return f"Token({self.token_type.name}, lexeme='{self.lexeme}', line={self.line}, literal={self.literal})"

    # def __eq__(self, other: Any) -> bool:
    #     if not isinstance(other, Token):
    #         return False
    #     return (
    #         self.token_type == other.token_type
    #         and self.lexeme == other.lexeme
    #         and self.line == other.line
    #         and self.literal == other.literal
    #     )
