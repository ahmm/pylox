import pathlib
import sys

from pylox.exceptions import ResolveError, RuntimeError
from pylox.interpreter import Interpreter
from pylox.parser import Parser
from pylox.resolver import Resolver
from pylox.scanner import Scanner
from pylox.utils import Singleton


class Lox(metaclass=Singleton):
    def __init__(self):
        self.had_error = False
        self.had_runtime_error = False

        self.interpreter = Interpreter()

    def run_file(self, path: pathlib.Path) -> None:
        with path.open() as f:
            self.run(f.read())
        if self.had_error:
            sys.exit(65)
        if self.had_runtime_error:
            sys.exit(70)

    def run_prompt(self) -> None:
        print(">>>>> Pylox Interactive Shell <<<<<")
        while True:
            try:
                line = input("> ")
                self.run(line)
                self.had_error = False
                self.had_runtime_error = False
            except (EOFError, KeyboardInterrupt):
                return

    def run(self, source: str) -> None:
        scanner = Scanner(source)
        tokens, lexical_errors = scanner.scan_tokens()

        parser = Parser(tokens)

        statements, parse_errors = parser.parse()
        for error in parse_errors:
            self.report_parse_error(error.token.line, f"at '{error.token.lexeme}'", error.message)
        if len(parse_errors):
            return

        try:
            self.interpreter.locals = Resolver().resolve(statements)
        except ResolveError as error:
            self.report_parse_error(error.token.line, f"at '{error.token.lexeme}'", error.message)
            return

        try:
            self.interpreter.interpret(statements)
        except RuntimeError as error:
            self.report_runtime_error(
                error.token.line, f"at '{error.token.lexeme}'", error.message
            )
            return

    def report_parse_error(self, line: int, where: str, message: str) -> None:
        print(f"[line {line}] Error {where}: {message}")
        self.had_error = True

    def report_runtime_error(self, line: int, where: str, message: str) -> None:
        print(f"[line {line}] Error {where}: {message}")
        self.had_runtime_error = True
