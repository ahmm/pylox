from pylox.token_type import Token


class LoxException(Exception):
    """Base class for all exceptions that we rise"""


class LexicalError(LoxException):
    def __init__(self, message: str, line: int):
        super().__init__(message)
        self.line = line


class ParseError(LoxException):
    def __init__(self, token: Token, message: str):
        self.token = token
        self.message = message


class RuntimeError(LoxException):
    def __init__(self, token: Token, message: str):
        self.token = token
        self.message = message


class ResolveError(LoxException):
    def __init__(self, token: Token, message: str):
        self.token = token
        self.message = message


class DivisionByZeroError(RuntimeError):
    pass
