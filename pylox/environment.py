from __future__ import annotations

from collections import UserDict
from typing import Any

from pylox.exceptions import RuntimeError
from pylox.token_type import Token


class Environment(UserDict):
    def __init__(self, enclosing: Environment | None = None):
        super().__init__()
        self.enclosing = enclosing

    def define(self, name: str, value: Any) -> None:
        self.data[name] = value

    def __getitem__(self, name: Token) -> Any:
        assert isinstance(name, Token), "Environment has to be called with a token"

        if name.lexeme in self.data:
            return self.data[name.lexeme]

        if self.enclosing is not None:
            return self.enclosing[name]

        raise RuntimeError(name, f"Undefined variable {name.lexeme}.")

    def __setitem__(self, name: Token, value: Any) -> None:
        """Set a variable value if already exists in the environment. Otherwise, raise a
        RuntimeError"""
        assert isinstance(name, Token), "Environment has to be called with a token"

        if name.lexeme in self.data:
            self.data[name.lexeme] = value
        elif self.enclosing is not None:
            self.enclosing[name] = value
        else:
            raise RuntimeError(name, f"Undefined variable {name.lexeme}.")

    def get_at(self, distance: int, name: Token) -> Any:
        return self._get_ancestor(distance)[name]

    def set_at(self, distance: int, name: Token, value: Any) -> None:
        self._get_ancestor(distance)[name] = value

    def _get_ancestor(self, distance: int) -> Environment:
        assert distance >= 0, "_get_ancestor cannot be called with negative distance"

        environment = self
        for i in range(distance):
            environment = environment.enclosing  # type: ignore
        return environment
