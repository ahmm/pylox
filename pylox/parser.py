from typing import Callable, Final, Optional, Type

from pylox.exceptions import ParseError
from pylox.expr import (
    AssignmentExpr,
    BinaryExpr,
    CallExpr,
    Expr,
    GroupingExpr,
    LiteralExpr,
    LogicalExpr,
    UnaryExpr,
    VariableExpr,
)
from pylox.stmt import (
    BlockStmt,
    ExpressionStmt,
    FunctionStmt,
    IfStmt,
    PrintStmt,
    ReturnStmt,
    Stmt,
    VarStmt,
    WhileStmt,
)
from pylox.token_type import Token, TokenType


class Parser:
    def __init__(self, tokens: list[Token]):
        self.tokens: Final[list[Token]] = tokens
        self.errors: list[ParseError] = []

        self.current = 0

    def parse(self) -> tuple[list[Stmt], list[ParseError]]:
        """grammar rule: program -> declaration* EOF"""
        statements = []
        while not self.is_at_end():
            if declaration := self.declaration():
                statements.append(declaration)
        return statements, self.errors

    # Methods for parsing specific statements
    def declaration(self) -> Optional[Stmt]:
        try:
            if self._advance_if_matches(TokenType.FUN):
                return self.function_declaration()
            if self._advance_if_matches(TokenType.VAR):
                return self.var_declaration()

            return self.statement()
        except ParseError as error:
            self.errors.append(error)
            self.synchronize()
            return None

    def var_declaration(self) -> VarStmt:
        if not self._advance_if_matches(TokenType.IDENTIFIER):
            raise ParseError(self.current_token, "Expect a variable name.")

        var_name = self.previous_token

        initializer = None
        if self._advance_if_matches(TokenType.EQUAL):
            initializer = self.expression()

        if not self._advance_if_matches(TokenType.SEMICOLON):
            raise ParseError(self.current_token, "Expect ; after variable declaration.")

        return VarStmt(var_name, initializer)

    def function_declaration(self) -> FunctionStmt:
        if not self._advance_if_matches(TokenType.IDENTIFIER):
            raise ParseError(self.current_token, "Expect function name.")
        name = self.previous_token

        if not self._advance_if_matches(TokenType.LEFT_PAREN):
            raise ParseError(self.current_token, "Expect '(' after function name.")

        parameters: list[Token] = []
        if self.current_token.token_type != TokenType.RIGHT_PAREN:
            if not self._advance_if_matches(TokenType.IDENTIFIER):
                raise ParseError(self.current_token, "Expect parameter name")
            parameters.append(self.previous_token)

            while self._advance_if_matches(TokenType.COMMA):
                if len(parameters) >= 255:
                    raise ParseError(self.current_token, "Can't have more than 255 arguments.")
                if not self._advance_if_matches(TokenType.IDENTIFIER):
                    raise ParseError(self.current_token, "Expect parameter name")
                parameters.append(self.previous_token)

        if not self._advance_if_matches(TokenType.RIGHT_PAREN):
            raise ParseError(self.current_token, "Expect '(' after function name.")

        if not self._advance_if_matches(TokenType.LEFT_BRACE):
            raise ParseError(self.current_token, "Expect '{' before function body.")

        body: list[Stmt] = self.block_statement()
        return FunctionStmt(name, parameters, body)

    def statement(self) -> Stmt:
        if self._advance_if_matches(TokenType.PRINT):
            return self.print_statement()
        if self._advance_if_matches(TokenType.RETURN):
            return self.return_statement()
        if self._advance_if_matches(TokenType.IF):
            return self.if_statement()
        if self._advance_if_matches(TokenType.WHILE):
            return self.while_statement()
        if self._advance_if_matches(TokenType.FOR):
            return self.for_statement()
        if self._advance_if_matches(TokenType.LEFT_BRACE):
            return BlockStmt(self.block_statement())

        return self.expression_statement()

    def if_statement(self) -> IfStmt:
        if not self._advance_if_matches(TokenType.LEFT_PAREN):
            raise ParseError(self.current_token, "Expect '(' after 'if'.")

        condition = self.expression()

        if not self._advance_if_matches(TokenType.RIGHT_PAREN):
            raise ParseError(self.current_token, "Expect ')' after if condition.")

        then_branch = self.statement()

        else_branch = None
        if self._advance_if_matches(TokenType.ELSE):
            else_branch = self.statement()

        return IfStmt(condition, then_branch, else_branch)

    def while_statement(self) -> WhileStmt:
        if not self._advance_if_matches(TokenType.LEFT_PAREN):
            raise ParseError(self.current_token, "Expect '(' after 'if'.")

        condition = self.expression()

        if not self._advance_if_matches(TokenType.RIGHT_PAREN):
            raise ParseError(self.current_token, "Expect ')' after if condition.")

        body = self.statement()

        return WhileStmt(condition, body)

    # forStmt        → "for" "(" ( varDecl | exprStmt | ";" )
    #                   expression? ";"
    #                   expression? ")" statement ;
    def for_statement(self) -> Stmt:
        """
        The `for` loop is just a syntax sugar for `while`, it does not have its own Stmt class.

        For example this statement:
            for (var i = 0; i < 5; i = i + 1) {
                print i;
            }
        becomes equivalent to:
            {
              var i = 0;
              while (i < 5) {
                { print i; }
                i += 1;
              }
            }
        """
        # (
        if not self._advance_if_matches(TokenType.LEFT_PAREN):
            raise ParseError(self.current_token, "Expect '(' after 'if'.")

        # initializer;
        initializer: Stmt | None = None
        if self._advance_if_matches(TokenType.SEMICOLON):
            pass  # just want to advance
        elif self._advance_if_matches(TokenType.VAR):
            initializer = self.var_declaration()
        else:
            initializer = self.expression_statement()

        # condition;
        condition = None
        if self.current_token.token_type != TokenType.SEMICOLON:
            condition = self.expression()

        if not self._advance_if_matches(TokenType.SEMICOLON):
            raise ParseError(self.current_token, "Expect ';' after loop condition")

        # increment
        increment = None
        if self.current_token.token_type != TokenType.RIGHT_PAREN:
            increment = self.expression()

        # )
        if not self._advance_if_matches(TokenType.RIGHT_PAREN):
            raise ParseError(self.current_token, "Expect ')' after if condition.")

        # Desugaring
        body = self.statement()
        if increment is not None:
            body = BlockStmt([body, ExpressionStmt(increment)])

        condition = condition or LiteralExpr(True)
        body = WhileStmt(condition, body)

        if initializer is not None:
            body = BlockStmt([initializer, body])

        return body

    def print_statement(self) -> PrintStmt:
        value = self.expression()

        if not self._advance_if_matches(TokenType.SEMICOLON):
            raise ParseError(self.current_token, "Expect ';' after value.")

        return PrintStmt(value)

    def return_statement(self) -> ReturnStmt:
        keyword = self.previous_token
        value: Expr | None = None
        if self.current_token.token_type != TokenType.SEMICOLON:
            value = self.expression()

        if not self._advance_if_matches(TokenType.SEMICOLON):
            raise ParseError(self.current_token, "Expect ';' after return value.")

        return ReturnStmt(keyword, value)

    def block_statement(self) -> list[Stmt]:
        statements = []

        while self.current_token.token_type not in [TokenType.RIGHT_BRACE, TokenType.EOF]:
            if declaration := self.declaration():
                statements.append(declaration)

        if not self._advance_if_matches(TokenType.RIGHT_BRACE):
            raise ParseError(self.current_token, "Expect '}' after block.")

        return statements

    def expression_statement(self) -> ExpressionStmt:
        value = self.expression()

        if not self._advance_if_matches(TokenType.SEMICOLON):
            raise ParseError(self.current_token, "Expect ';' after value.")

        return ExpressionStmt(value)

    # Methods for parsing specific expressions
    def expression(self) -> Expr:
        return self.assignment()

    def assignment(self) -> Expr:
        expr = self.or_expr()  # parse a rule of higher precedence

        if self._advance_if_matches(TokenType.EQUAL):
            equals = self.previous_token
            value = self.assignment()  # right associative

            if not isinstance(expr, VariableExpr):
                raise ParseError(equals, "Invalid assignment target.")

            # we convert VariableExpr -> AssignmentExpr (r-value -> l-value)
            return AssignmentExpr(expr.name, value)

        return expr

    def or_expr(self) -> Expr:
        return self._parse_left_associative_series(
            [TokenType.OR], self.and_expr, expr_class=LogicalExpr
        )

    def and_expr(self) -> Expr:
        return self._parse_left_associative_series(
            [TokenType.AND], self.equality, expr_class=LogicalExpr
        )

    def equality(self) -> Expr:
        """grammar rule: equality -> comparison ( ( "!=" | "==" ) comparison )*"""
        return self._parse_left_associative_series(
            [TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL], self.comparison
        )

    def comparison(self) -> Expr:
        """grammar rule: comparison -> term ( ( ">" | ">=" | "<" | "<=" ) term )*"""
        return self._parse_left_associative_series(
            [
                TokenType.GREATER,
                TokenType.GREATER_EQUAL,
                TokenType.LESS,
                TokenType.LESS_EQUAL,
            ],
            self.term,
        )

    def term(self) -> Expr:
        """grammar rule: term -> factor ( ( "-" | "+" ) factor )*"""
        return self._parse_left_associative_series([TokenType.PLUS, TokenType.MINUS], self.factor)

    def factor(self) -> Expr:
        """grammar rule: factor -> unary ( ( "/" | "*" ) factor )*"""
        return self._parse_left_associative_series([TokenType.STAR, TokenType.SLASH], self.unary)

    def unary(self) -> Expr:
        """grammar rule: unary -> ( "!" | "-" ) unary | primary"""
        while self.current_token.token_type in [TokenType.BANG, TokenType.MINUS]:
            operator = self.current_token
            self._advance()
            right = self.unary()
            return UnaryExpr(operator, right)

        return self.call()

    def call(self) -> Expr:
        expr = self.primary()

        while True:
            if self._advance_if_matches(TokenType.LEFT_PAREN):
                expr = self._finish_call(expr)
            else:  # we keep this else here for when we'll be implementing classes
                break

        return expr

    def primary(self) -> Expr:
        """
        grammar rule: primary -> "true" | "false" | "nil" | "this"
                               | NUMBER | STRING | IDENTIFIER | "(" expression ")"
                               | "super" "." IDENTIFIER
        """
        match self.current_token.token_type:
            # Match single terminals
            case TokenType.FALSE:
                self._advance()
                return LiteralExpr(False)
            case TokenType.TRUE:
                self._advance()
                return LiteralExpr(True)
            case TokenType.NIL:
                self._advance()
                return LiteralExpr(None)
            case TokenType.NUMBER | TokenType.STRING:
                self._advance()
                return LiteralExpr(self.previous_token.literal)
            case TokenType.LEFT_PAREN:
                self._advance()
                expr = self.expression()  # sigh... here we go again
                if self.current_token.token_type != TokenType.RIGHT_PAREN:
                    raise ParseError(self.current_token, "Expect ')' after expression.")
                self._advance()
                return GroupingExpr(expr)
            case TokenType.IDENTIFIER:
                self._advance()
                return VariableExpr(self.previous_token)
            case _:
                raise ParseError(self.current_token, "Expect expression.")

    def synchronize(self) -> None:
        """Discard tokens until the next statement."""
        self._advance()

        while not self.is_at_end():
            if self.previous_token.token_type == TokenType.SEMICOLON:
                return

            if self.current_token.token_type in [
                TokenType.CLASS,
                TokenType.FUN,
                TokenType.VAR,
                TokenType.FOR,
                TokenType.IF,
                TokenType.WHILE,
                TokenType.PRINT,
                TokenType.RETURN,
            ]:
                return

            self._advance()

    def _finish_call(self, callee: Expr) -> CallExpr:
        arguments = []

        # if the next token is ')' we don't try to parse any arguments
        if self.current_token.token_type != TokenType.RIGHT_PAREN:
            # otherwise parse the first argument
            arguments.append(self.expression())
            # then any additional arguments
            while self._advance_if_matches(TokenType.COMMA):
                if len(arguments) >= 255:
                    raise ParseError(self.current_token, "Can't have more than 255 arguments.")
                arguments.append(self.expression())

        if not self._advance_if_matches(TokenType.RIGHT_PAREN):
            raise ParseError(self.current_token, "Expect ')' after arguments.")

        return CallExpr(callee, self.previous_token, arguments)

    def _parse_left_associative_series(
        self,
        token_types: list[TokenType],
        operand_method: Callable[[], Expr],
        *,
        expr_class: Type[BinaryExpr | LogicalExpr] = BinaryExpr,
    ) -> Expr:
        expr = operand_method()

        while self.current_token.token_type in token_types:
            operator = self.current_token
            self._advance()
            right = operand_method()
            expr = expr_class(expr, operator, right)
        return expr

    @property
    def current_token(self) -> Token:
        return self.tokens[self.current]

    @property
    def previous_token(self) -> Token:
        return self.tokens[self.current - 1]

    def _advance_if_matches(self, *token_types: TokenType) -> bool:
        """Check if current token matches one of the expected types and maybe advance."""
        if self.current_token.token_type in token_types:
            self._advance()
            return True
        return False

    def _advance(self) -> None:
        if not self.is_at_end():
            self.current += 1

    def is_at_end(self) -> bool:
        return self.current_token.token_type == TokenType.EOF
