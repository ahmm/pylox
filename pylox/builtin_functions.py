import time
from typing import Any

from pylox.function import Callable


class Clock(Callable):
    @property
    def arity(self) -> int:
        return 0

    def call(self, *arguments: Any) -> float:
        return time.time() / 1000.0

    def __str__(self):
        return "<native fn 'clock'>"


class Print(Callable):
    @property
    def arity(self) -> int:
        return 1

    def call(self, *arguments: Any) -> None:
        for argument in arguments:
            print(argument)

    def __str__(self):
        return "<native fn 'clock'>"
