import pytest

from pylox.ast_printer import AstPrinter
from pylox.parser import Parser
from pylox.scanner import Scanner
from pylox.token_type import TokenType


def test_parsing_expressions():
    scanner = Scanner("-1 * (2 + 3 - 4) / 10")
    tokens, _ = scanner.scan_tokens()

    parser = Parser(tokens)
    result = AstPrinter().print(parser.expression())
    assert result == "(/ (* (- 1.0 ) (group (- (+ 2.0 3.0 ) 4.0 ) ) ) 10.0 )"


@pytest.mark.parametrize(
    "source,expected_type,expected_message",
    [
        pytest.param("( 4", TokenType.EOF, "Expect ')' after expression."),
        pytest.param("1 + 2 * ( 4", TokenType.EOF, "Expect ')' after expression."),
    ],
)
def test_errors(source: str, expected_type: TokenType, expected_message: str):
    scanner = Scanner(source)
    tokens, _ = scanner.scan_tokens()
    parser = Parser(tokens)

    _, errors = parser.parse()

    parse_error = errors[0]
    assert parse_error.token.token_type == expected_type
    assert parse_error.message == expected_message
