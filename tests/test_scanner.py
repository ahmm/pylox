import pytest

from pylox.scanner import Scanner
from pylox.token_type import Token, TokenType


@pytest.mark.parametrize(
    "source,expected_tokens,expected_errors",
    [
        pytest.param(
            "2 * /* 3 + 3 */ 4",
            [
                Token(TokenType.NUMBER, "2", 1, literal=2.0),
                Token(TokenType.STAR, "*", 1),
                Token(TokenType.NUMBER, "4", 1, literal=4.0),
                Token(TokenType.EOF, "", 1),
            ],
            [],
            id="Correct code 1",
        ),
        pytest.param(
            '/* This should all \nbe ignored\n*/print "hello"',
            [
                Token(TokenType.PRINT, "print", 3),
                Token(TokenType.STRING, '"hello"', 3, literal="hello"),
                Token(TokenType.EOF, "", 3),
            ],
            [],
            id="Correct code 2",
        ),
        pytest.param(
            "/* ignored stuff",
            [Token(TokenType.EOF, "", 1)],
            ["[Line 1] Error: Unterminated multiline comment"],
            id="Unterminated error 1",
        ),
        pytest.param(
            "/* ignored stuff * /",
            [Token(TokenType.EOF, "", 1)],
            ["[Line 1] Error: Unterminated multiline comment"],
            id="Unterminated error 2",
        ),
        pytest.param(
            "/* ignored stuff *",
            [Token(TokenType.EOF, "", 1)],
            ["[Line 1] Error: Unterminated multiline comment"],
            id="Unterminated error 3",
        ),
        pytest.param(
            "/* ignored stuff * more stuff",
            [Token(TokenType.EOF, "", 1)],
            ["[Line 1] Error: Unterminated multiline comment"],
            id="Unterminated error 4",
        ),
    ],
)
def test_multiline_comments(
    source: str, expected_tokens: list[Token], expected_errors: list[str]
):
    def compare_token(a: Token, b: Token) -> bool:
        return a.token_type == b.token_type and a.literal == b.literal

    scanner = Scanner(source)
    tokens, errors = scanner.scan_tokens()

    assert errors == expected_errors

    for token, expected_token in zip(tokens, expected_tokens):
        # for the purposes of this test we don't care about lines etc.
        assert compare_token(token, expected_token)
