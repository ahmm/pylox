import pytest

from pylox.lox import Lox


@pytest.mark.parametrize(
    "source_code,expected_output",
    [
        (
            """print "one";
               print true;
               print 2 + 1;""",
            "one\ntrue\n3\n",
        ),
        (
            """var flag = true;
               print flag;""",
            "true\n",
        ),
        (
            """var firstname = "Bob";
               var surname = "Lablaw";
               var name = firstname + " " + surname;
               print name;""",
            "Bob Lablaw\n",
        ),
        (
            """var firstname = "Bob";
               var surname = "Lablaw";
               print firstname + " " + surname;""",
            "Bob Lablaw\n",
        ),
        (
            """var a = "global a";
            var b = "global b";
            var c = "global c";
            {
              var a = "outer a";
              var b = "outer b";
              {
                var a = "inner a";
                print a;
                print b;
                print c;
              }
              print a;
              print b;
              print c;
            }
            print a;
            print b;
            print c;""",
            (
                "inner a\nouter b\nglobal c\n"
                "outer a\nouter b\nglobal c\n"
                "global a\nglobal b\nglobal c\n"
            ),
        ),
        (
            """
           var a = "global";
           {
               fun show_a() {
                    print(a);
               }

               show_a();
               var a = "block";
               show_a();
           }""",
            "global\nglobal\n",
        ),
    ],
)
def test_variables_and_scope(source_code: str, expected_output: str, capsys):
    Lox().run(source_code)

    capture = capsys.readouterr()
    assert capture.out == expected_output


@pytest.mark.parametrize(
    "source_code,expected_output",
    [
        (
            """
            var a = 10;
            var b = 4;
            if (a > b) {
                print "a is greater than b";
            } else {
                print "a is smaller or equal to b";
            }
            """,
            "a is greater than b\n",
        ),
        (
            """
            var a = 1;
            var b = 4;
            if (a > b) {
                print "a is greater than b";
            } else {
                print "a is smaller or equal to b";
            }
            """,
            "a is smaller or equal to b\n",
        ),
        (
            """
            var bool_flag = true;
            if (bool_flag) {
                print "the bool flag is true";
            } else {
                print "the bool flag is false";
            }
            """,
            "the bool flag is true\n",
        ),
        (
            """
            var bool_flag = false;
            if (bool_flag) {
                print "the bool flag is true";
            } else {
                print "the bool flag is false";
            }
            """,
            "the bool flag is false\n",
        ),
        (
            """
            var bool_flag = nil;
            if (bool_flag) {
                print "print only if true";
            } 
            print "print either way";
            """,
            "print either way\n",
        ),
        (
            """
            if (true and "string") {
                print "should print this";
            }
            """,
            "should print this\n",
        ),
        (
            """
            if (false or true) {
                print "should print this";
            }
            """,
            "should print this\n",
        ),
        (
            """
            if (true or false) {
                print "should print this";
            }
            """,
            "should print this\n",
        ),
        (
            """
            if ("string" and 44 and 77 and true) {
                print "should print this";
            }
            """,
            "should print this\n",
        ),
        (
            """
            if ("string" and 44 and 77 and false) {
                print "should not print this";
            }
            print "should only print this";
            """,
            "should only print this\n",
        ),
        (
            """
            if (false or nil or !false) {
                print "should print this";
            }
            """,
            "should print this\n",
        ),
    ],
)
def test_if_statement(source_code: str, expected_output: str, capsys):
    Lox().run(source_code)

    capture = capsys.readouterr()
    assert capture.out == expected_output


@pytest.mark.parametrize(
    "source_code,expected_output",
    [
        (
            """
            var i = 0;
            while (i < 5) {
                print "*";
                i = i + 1;
            }
            """,
            "*\n*\n*\n*\n*\n",
        ),
        (
            """
            var i = 0;
            while (i < 1000) i = i + 1;
            print i;
            """,
            "1000\n",
        ),
        (
            """
            while (false) { print "should not be printed"; }
            print "only this should be printed";
            """,
            "only this should be printed\n",
        ),
    ],
)
def test_while_statement(source_code: str, expected_output: str, capsys):
    Lox().run(source_code)

    capture = capsys.readouterr()
    assert capture.out == expected_output


@pytest.mark.parametrize(
    "source_code,expected_output",
    [
        (
            """
                for (var i = 0; i < 5; i = i + 1) {
                    print "*";
                }
                """,
            "*\n*\n*\n*\n*\n",
        ),
    ],
)
def test_for_loop(source_code: str, expected_output: str, capsys):
    Lox().run(source_code)
    capture = capsys.readouterr()
    assert capture.out == expected_output


@pytest.mark.parametrize(
    "source_code,expected_output",
    [
        (
            """
            fun fib(n) {
              if (n <= 1) return n;
                return fib(n - 2) + fib(n - 1);
            }

            for (var i = 0; i < 10; i = i + 1) {
              print fib(i);
            }
            """,
            "0\n1\n1\n2\n3\n5\n8\n13\n21\n34\n",
        ),
        (
            """
            fun say_hi(first, last) {
              print "Hello " + first + " " + last + "!";
            }

            say_hi("Andrzej", "Duda");
            """,
            "Hello Andrzej Duda!\n",
        ),
    ],
)
def test_functions(source_code: str, expected_output: str, capsys):
    Lox().run(source_code)
    capture = capsys.readouterr()
    assert capture.out == expected_output
