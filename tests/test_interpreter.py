from pylox.interpreter import is_equal, is_truthy, stringify


def test_is_truthy_falsy_values():
    values = [False, None]

    for value in values:
        falsy_value = is_truthy(value)
        assert isinstance(falsy_value, bool) and falsy_value is False


def test_is_truthy_truthy_values():
    values = ["", 0, [], {}, True]

    for value in values:
        truthy_value = is_truthy(value)
        assert truthy_value is True


def test_is_equal_false():
    values = [(False, True), (False, None), (2, "2"), (0, None), (0, False)]

    for a, b in values:
        result = is_equal(a, b)
        assert result is False, f"{a} and {b} should not be equal"


def test_is_equal_true():
    values = [("2", "2"), (3, 3), (None, None), (False, False), (True, True)]

    for a, b in values:
        result = is_equal(a, b)
        assert result is True, f"{a} and {b} should be equal"


def test_stringify():
    values = [(None, "nil"), (2.0, "2"), (2.3, "2.3"), (False, "false"), (True, "true")]

    for value, expected_string in values:
        assert stringify(value) == expected_string
